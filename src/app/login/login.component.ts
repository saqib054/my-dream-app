import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  login = false;
  loggedInUser;
  userExist;
  userData = [
    {
      email: "user@gmail.com",
      role: "user",
    },
    {
      email: "admin@gmail.com",
      role: "admin",
    },
  ];
  constructor() {}

  ngOnInit() {}

  userForm = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null),
  });

  onLoginFormSubmit() {
    this.loggedInUser = this.userData.find((user) => {
      return user.email === this.userForm.value.email;
    });

    if (this.loggedInUser) {
      this.login = true;
    }
  }

  logout() {
    this.login = false;
    this.userForm.reset();
    this.loggedInUser = null;
  }
}
